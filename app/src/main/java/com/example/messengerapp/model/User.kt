package com.example.messengerapp.model

data class User(val name: String, val profileImage: String) {
    constructor() : this("", "")
}