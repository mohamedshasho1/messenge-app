package com.example.messengerapp.model

import java.util.*

data class ImageMessage(
    val imagePath: String,
    override val senderId: String,
    override val recipientId: String,
    override val date: Date,
    override val type: MessageType = MessageType.IMAGE
) : Message