package com.example.messengerapp.model

enum class MessageType {
    TEXT,
    IMAGE
}